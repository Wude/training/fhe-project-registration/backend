# Requirements

## Use cases

Roles:

* Student
* Lecturer
* Secretariat
* Board of examiners
* Examination Authority
* Administrator

Multi tenancy for faculties/departments.
Each faculty has students, lecturers, sekretariat staff and members of the board of examiners.

General functionality:

* "Signature" with digital pens
* Usage of digital certificates
* Display of projects/theses by their state (accepted, deadline extended, graded, completed, ...)
* Web based frontend (not yet implemented)
* Generation of documents (not yet implemented, maybe export/backup)
* Notifications for important dates and events for all roles (not yet implemented)

### Student

* View own projects and theses
* Crete project or thesis and digitally sign
* Entry of titel, supervising tutor
* View the status of own project and theses
* Deposit documents
* Request deadline extension

![Use cases: Student](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/use-case-student-en.svg)

### Lecturer

* View supervised projects and theses
* Accept or reject a project or thesis
* Change titel, deadline
* Forward to a possible seconds supervising tutor
* Forward to the secreteriat when all supervising tutor have accepted
* Close a project or thesis
* Store the grading for a project or thesis

![Use cases: Lecturer](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/use-case-lecturer-en.svg)

### Secreteriat

* View accepted projects and theses
* Formally register projects and theses
* Projekte/Arbeiten als "vom Studierenden abgegeben" markieren
* Mark projects and theses as "delivered" by the students

![Use cases: Secreteriat](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/use-case-secretariat-en.svg)

### Board of examiners

* View projects and theses for which deadline extensions were requested
* Grant or reject deadline extensions for projects and theses

![Use cases: Board of examiners](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/use-case-boardOfExaminers-en.svg)

### Examination Authority

* View status and grading for projects and theses
* Mark projects and theses as completed

![Use cases: Examination Authority](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/use-case-examinationAuthority-en.svg)

### Administrator

* All functionality of other role & general system management

![Use cases: Examination Authority](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/use-case-administrator-en.svg)

## Workflow

![Workflow](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/sequence-en.svg)

## Entity data types

![Class Details](https://gitlab.com/Wude/fhe-project-registration/-/raw/master/doc/images/class-detail.svg)
