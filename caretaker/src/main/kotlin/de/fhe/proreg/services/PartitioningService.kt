package de.fhe.proreg.services

import de.fhe.proreg.shared.format
import io.quarkus.scheduler.Scheduled
import io.quarkus.scheduler.ScheduledExecution
import java.sql.Connection
import java.sql.SQLException
import java.sql.Statement
import java.time.Instant
import java.time.LocalDate
import java.time.temporal.ChronoField
import jakarta.enterprise.context.ApplicationScoped
import javax.sql.DataSource
import org.jboss.logging.Logger

data class TablePartitionRangeInfo(
    val mainTableName: String,
    val tablePartitionName: String,
    val startValue: String,
    val endValue: String,
) {
    companion object {
        fun byYear(tableName: String, yearsAhead: Int = 0): TablePartitionRangeInfo {
            val year = Instant.now().get(ChronoField.YEAR) + yearsAhead
            val start = LocalDate.of(year, 1, 1)
            val end = LocalDate.of(year + 1, 1, 1)

            return TablePartitionRangeInfo(
                mainTableName = tableName,
                tablePartitionName = "${tableName}_${year}",
                startValue = "'${start.format()}'",
                endValue = "'${end.format()}'")
        }
    }
}

@ApplicationScoped
open class PartitioningService(
    val log: Logger,
    val dataSource: DataSource,
) {
    /* Table partition descriptions */
    val TablePartitionRangeInfoGetters: List<() -> TablePartitionRangeInfo> = listOf(
        { TablePartitionRangeInfo.byYear("projects") },
        { TablePartitionRangeInfo.byYear("projects", yearsAhead = 1) },
        { TablePartitionRangeInfo.byYear("project_documents") },
        { TablePartitionRangeInfo.byYear("project_documents", yearsAhead = 1) },
    )

    @Scheduled(
        identity = "Partitioning",
        cron = "{de.fhe.proreg.caretaker-partitioning}",
        timeZone = "Europe/Berlin")
    fun createTablePartitions(/*ScheduledExecution execution*/) {
        try {
            val conn: Connection = dataSource.getConnection()
            conn.use {
                for (getter in TablePartitionRangeInfoGetters) {
                    createTablePartitionRange(conn, getter())
                }
            }
        } catch (e: Exception) {
            log.error("Table partition creation failed", e)
        }
    }

    fun createTablePartitionRange(
        conn: Connection,
        info: TablePartitionRangeInfo,
    ): Boolean {
        if (!tableExists(conn, info.tablePartitionName)) {
            try {
                val stmt: Statement = conn.createStatement()
                stmt.execute("""
                    create table ${info.tablePartitionName}
                    partition of ${info.mainTableName}
                    for values from (${info.startValue}) to (${info.endValue})
                """.trimIndent())
                conn.commit()
                return true
            } catch (e: SQLException) {
                if (e.message != null && e.message!!.contains("already exists")) {
                    log.warn("The table partition already exists")
                } else {
                    log.error("Table partition creation failed", e)
                }
            } catch (e: Exception) {
                log.error("Table partition creation failed", e)
            }
            conn.rollback()
        }
        return false
    }

    fun tableExists(conn: Connection, tableName: String): Boolean {
        try {
            val preparedStatement = conn.prepareStatement("""
                    select  count(*)
                    from    information_schema.tables
                    where   table_name = ?
                    limit   1
                """.trimIndent())
            preparedStatement.setString(1, tableName)

            val resultSet = preparedStatement.executeQuery()
            resultSet.next()
            return resultSet.getInt(1) != 0
        } catch (e: SQLException) {
            log.error("Table info query failed", e)
        }
        return false
    }
}
