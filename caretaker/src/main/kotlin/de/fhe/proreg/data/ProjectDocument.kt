package de.fhe.proreg.data

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
// import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import java.time.Instant
import java.util.UUID
import jakarta.persistence.Cacheable
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.ForeignKey
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.PrePersist
import jakarta.persistence.Table
import jakarta.persistence.UniqueConstraint
import jakarta.persistence.FetchType
import jakarta.enterprise.context.ApplicationScoped

@Entity
@Cacheable
@Table(name = "project_documents", uniqueConstraints = [UniqueConstraint(name = "uk_project_documents", columnNames = ["title"])])
open class ProjectDocument : EntityBase() {

    @Id
    @GeneratedValue
    @Column(name = "project_document_id", nullable = false, updatable = false)
    open var projectDocumentID: UUID? = null

    // @Column(name = "project_id", nullable = false, insertable = true, updatable = false)
    // lateinit open var projectID: UUID

    @ManyToOne(optional = false, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false, insertable = true, updatable = false,
        foreignKey = ForeignKey(name = "fk_projects_document_p_id"))
    lateinit open var project: Project

    @Column(name = "title", nullable = false)
    lateinit open var title: String

    @Column(name = "content", nullable = false)
    lateinit open var content: ByteArray

    @PrePersist
    fun onPrePersist() {
        if (projectDocumentID == null) {
            projectDocumentID = UUID.randomUUID()
        }
    }
}

@ApplicationScoped
class ProjectDocumentRepository : RepositoryBase<ProjectDocument, UUID>() { }
