package de.fhe.proreg.data

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
import io.quarkus.panache.common.Sort
import io.smallrye.mutiny.Multi
import de.fhe.proreg.shared.TimeUtils
import java.time.Instant
import java.time.LocalDate
import java.util.UUID
import java.math.BigDecimal
import jakarta.persistence.AttributeConverter
import jakarta.persistence.Cacheable
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Convert
import jakarta.persistence.Converter
import jakarta.persistence.Entity
import jakarta.persistence.ForeignKey
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.Index
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import jakarta.persistence.PrePersist
import jakarta.persistence.Table
import jakarta.persistence.FetchType
import jakarta.enterprise.context.ApplicationScoped

enum class ProjectType(val code: String) {
    BACHELORS_PROJECT("BAP"),
    BACHELORS_THESIS("BAT"),
    MASTERS_PROJECT("MAP"),
    MASTERS_THESIS("MAT");

    companion object {

        val valuesByCode = HashMap<String, ProjectType>()

        init {
            val projectTypeValues = ProjectType.values()

            for (projectType in projectTypeValues) {
                valuesByCode.put(projectType.code, projectType);
                valuesByCode.put(projectType.name, projectType);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: ProjectType): ProjectType = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): ProjectType? = valuesByCode.get(idOrCode)
    }
}

enum class ProjectStatus(val code: String) {
    SUBMITTED("SUB"),
    REJECTED("REJ"),
    ACCEPTED("ACC"),
    REGISTERED("REG"),
    DEADLINE_EXTENSION_REQUESTED("DEQ"),
    DEADLINE_EXTENSION_REJECTED("DEJ"),
    DEADLINE_EXTENSION_GRANTED("DEG"),
    GRADED("GRA"),
    COMPLETED("COM");

    companion object {

        val valuesByCode = HashMap<String, ProjectStatus>()

        init {
            val projectStatusValues = ProjectStatus.values()

            for (projectStatus in projectStatusValues) {
                valuesByCode.put(projectStatus.code, projectStatus);
                valuesByCode.put(projectStatus.name, projectStatus);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: ProjectStatus): ProjectStatus = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): ProjectStatus? = valuesByCode.get(idOrCode)
    }
}

@Converter(autoApply = true)
class ProjectTypeConverter : AttributeConverter<ProjectType, String> {

    override fun convertToDatabaseColumn(projectType: ProjectType?): String? = if (projectType == null) { null } else { projectType.code }
    override fun convertToEntityAttribute(code: String?): ProjectType? = ProjectType.tryValueOf(code)
}

@Converter(autoApply = true)
class ProjectStatusConverter : AttributeConverter<ProjectStatus, String> {

    override fun convertToDatabaseColumn(projectStatus: ProjectStatus?): String? = if (projectStatus == null) { null } else { projectStatus.code }
    override fun convertToEntityAttribute(code: String?): ProjectStatus? = ProjectStatus.tryValueOf(code)
}

@Entity
@Cacheable
@Table(name = "projects", indexes = [
    Index(name = "ix_projects_cre_instant", columnList = "creation_instant"),
    Index(name = "ix_projects_mod_instant", columnList = "modification_instant"),
    Index(name = "ix_projects_status", columnList = "status"),
])
open class Project : EntityBase() {

    @Id
    @GeneratedValue
    @Column(name = "project_id", nullable = false, updatable = false)
    open var projectID: UUID? = null

    // @Column(name = "student_id", nullable = false)
    // lateinit open var studentID: UUID

    @ManyToOne(optional = false, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", nullable = false, foreignKey = ForeignKey(name = "fk_projects_student_id"))
    lateinit open var student: User

    // @Column(name = "supervising_tutor_1_id", nullable = false)
    // lateinit open var supervisingTutor1ID: UUID

    @ManyToOne(optional = false, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "supervising_tutor_1_id", nullable = false, foreignKey = ForeignKey(name = "fk_projects_tutor_1_id"))
    lateinit open var supervisingTutor1: User

    // @Column(name = "supervising_tutor_2_id", nullable = true)
    // lateinit open var supervisingTutor2ID: UUID?

    @ManyToOne(optional = true, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "supervising_tutor_2_id", nullable = true, foreignKey = ForeignKey(name = "fk_projects_tutor_2_id"))
    open var supervisingTutor2: User? = null

    @Column(name = "supervising_tutor_1_accepted", nullable = false)
    open var supervisingTutor1Accepted: Boolean = false

    @Column(name = "supervising_tutor_2_accepted", nullable = false)
    open var supervisingTutor2Accepted: Boolean = false

    @Column(name = "student_signature", nullable = false)
    lateinit open var studentSignature: ByteArray

    @Column(name = "supervising_tutor_1_signature", nullable = true)
    open var supervisingTutor1Signature: ByteArray? = null

    @Column(name = "supervising_tutor_2_signature", nullable = true)
    open var supervisingTutor2Signature: ByteArray? = null

    @Convert(converter = ProjectTypeConverter::class)
    @Column(name = "project_type", columnDefinition = "char(3)", nullable = false)
    lateinit open var projectType: ProjectType

    @Column(name = "title", nullable = false)
    lateinit open var title: String

    @Column(name = "grade", nullable = true, precision = 2, scale = 1)
    open var grade: BigDecimal? = null

    @Column(name = "deadline", nullable = true)
    open var deadline: LocalDate? = null

    @Convert(converter = ProjectStatusConverter::class)
    @Column(name = "status", nullable = false, columnDefinition = "char(3)")
    lateinit open var status: ProjectStatus

    @OneToMany(targetEntity = ProjectDocument::class, orphanRemoval = true, mappedBy = "project")
    lateinit open var documents: List<ProjectDocument>

    // @Column(name = "faculty_acronym", nullable = false, insertable = true, updatable = false)
    // lateinit open var facultyAcronym: String

    @ManyToOne(optional = false, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "faculty_acronym", nullable = false, insertable = true, updatable = false,
        foreignKey = ForeignKey(name = "fk_projects_f_acronym"))
    lateinit open var faculty: Faculty

    // @Column(name = "department_acronym", nullable = false, insertable = true, updatable = false)
    // lateinit open var departmentAcronym: String

    @ManyToOne(optional = false, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "department_acronym", nullable = false, insertable = true, updatable = false,
        foreignKey = ForeignKey(name = "fk_projects_d_acronym"))
    lateinit open var department: Department

    @PrePersist
    fun onPrePersist() {
        if (projectID == null) {
            projectID = UUID.randomUUID()
        }
    }
}

@ApplicationScoped
class ProjectRepository : RepositoryBase<Project, UUID>() {

    // fun findByStudentID(studentID: UUID) = findStream("student_id", studentID)

    fun findByYear(
        pageIndex: Int,
        pageSize: Int,
        year: Int,
    ): Multi<Project> {
        val startTime = TimeUtils.germanInstantOf(year, 1, 1)
        val endTime = TimeUtils.germanInstantOf(year + 1, 1, 1)
        return findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "creationInstant >= ?1 and creationInstant < ?2",
            startTime,
            endTime)
    }

    fun findByYearAndDepartment(
        pageIndex: Int,
        pageSize: Int,
        year: Int,
        departmentAcronym: String,
    ): Multi<Project> {
        val startTime = TimeUtils.germanInstantOf(year, 1, 1)
        val endTime = TimeUtils.germanInstantOf(year, 1, 1)
        return findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "creationInstant >= ?1 and creationInstant < ?2 and department.acronym = ?3",
            startTime,
            endTime,
            departmentAcronym)
    }

    fun findByStudentID(pageIndex: Int, pageSize: Int, studentID: UUID) =
        findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "student.userID",
            studentID)

    fun findByLecturerID(pageIndex: Int, pageSize: Int, lecturerID: UUID) =
        findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "supervisingTutor1.userID = ?1 or supervisingTutor2.userID = ?1",
            lecturerID)

    // fun findBySupervisingTutor1ID(lecturerID: UUID) = findStream("supervisingTutor1ID", lecturerID)

    fun findBySupervisingTutor1ID(pageIndex: Int, pageSize: Int, lecturerID: UUID) =
        findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "supervisingTutor1.userID",
            lecturerID)

    // fun findBySupervisingTutor2ID(lecturerID: UUID) = findStream("supervisingTutor2ID", lecturerID)

    fun findBySupervisingTutor2ID(pageIndex: Int, pageSize: Int, lecturerID: UUID) =
        findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "supervisingTutor2.userID",
            lecturerID)

    fun viewProjectsByStatus(
        pageIndex: Int,
        pageSize: Int,
        projectStatus: ProjectStatus,
        facultyAcronym: String,
    ) = findPageStream(
            pageIndex,
            pageSize,
            OrderByCreationInstantAsc,
            "projectStatus = ?1 and facultyAcronym = ?2",
            projectStatus,
            facultyAcronym)

    fun viewProjectsByStatus(pageIndex: Int, pageSize: Int, projectStatus: ProjectStatus) =
        findPageStream(pageIndex, pageSize, OrderByCreationInstantAsc, "projectStatus", projectStatus)

    fun findByFacultyAcronym(pageIndex: Int, pageSize: Int, facultyAcronym: String) =
        findPageStream(pageIndex, pageSize, OrderByCreationInstantAsc, "facultyAcronym", facultyAcronym)

    fun findByDepartmentAcronym(pageIndex: Int, pageSize: Int, departmentAcronym: String) =
        findPageStream(pageIndex, pageSize, OrderByCreationInstantAsc, "departmentAcronym", departmentAcronym)
}
