package de.fhe.proreg.data

// import de.fhe.proreg.shared.Utils
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
// import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import java.time.Instant
import jakarta.persistence.Cacheable
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.ForeignKey
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.JoinTable
import jakarta.persistence.ManyToMany
import jakarta.persistence.OneToMany
import jakarta.persistence.PrePersist
import jakarta.persistence.Table
import jakarta.persistence.JoinColumn
import jakarta.enterprise.context.ApplicationScoped
import org.hibernate.annotations.NaturalId

@Entity
@Cacheable
@Table(name = "faculties")
open class Faculty : EntityBase() {

    @Id
    @NaturalId
    @Column(name = "faculty_acronym", columnDefinition = "char(3)", nullable = false, updatable = false)
    open var acronym: String? = null

    @Column(name = "name", nullable = false)
    lateinit open var name: String

    @OneToMany(targetEntity = Department::class, orphanRemoval = true, mappedBy = "faculty")
    lateinit open var departments: List<Department>

    @OneToMany(targetEntity = Project::class, orphanRemoval = true, mappedBy = "faculty")
    lateinit open var projects: List<Project>

    @ManyToMany(targetEntity = User::class, cascade = [CascadeType.ALL])
    @JoinTable(name = "user_faculties",
        joinColumns = [JoinColumn(name = "faculty_acronym", referencedColumnName = "faculty_acronym")],
        inverseJoinColumns = [JoinColumn(name = "user_id", referencedColumnName = "user_id")],
        foreignKey = ForeignKey(name = "fk_user_faculties_f_acr"),
        inverseForeignKey = ForeignKey(name = "fk_user_faculties_u_id"),
    )
    lateinit open var users: List<User>
}

@ApplicationScoped
class FacultyRepository : RepositoryBase<Faculty, String>() { }
