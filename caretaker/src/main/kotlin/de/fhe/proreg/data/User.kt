package de.fhe.proreg.data

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
import io.quarkus.panache.common.Sort
import io.smallrye.mutiny.Multi
import java.time.Instant
import java.util.UUID
import jakarta.persistence.AttributeConverter
import jakarta.persistence.Cacheable
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Convert
import jakarta.persistence.Converter
import jakarta.persistence.Entity
import jakarta.persistence.ForeignKey
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.Index
import jakarta.persistence.JoinTable
import jakarta.persistence.JoinColumn
import jakarta.persistence.OneToMany
import jakarta.persistence.ManyToMany
import jakarta.persistence.PrePersist
import jakarta.persistence.Table
import jakarta.persistence.UniqueConstraint
import jakarta.enterprise.context.ApplicationScoped

/**
* The groups a user may belong to.
*/
object UserGroup {

    /**
    * A student.
    */
    const val STUDENT = "STU"

    /**
    * A lecturer.
    */
    const val LECTURER = "LEC"

    /**
    * A lecturer who is also a member of the faculty board of examiners.
    */
    const val FACULTY_BOARD_OF_EXAMINERS = "FBE"

    /**
    * A staff member of the faculty secreteriat.
    */
    const val FACULTY_SECRETARIAT_STAFF = "FSS"

    /**
    * A staff member of the examination authority.
    */
    const val EXAMINATION_AUTHORITY_STAFF = "EAS"

    /**
    * An administrator.
    */
    const val ADMINISTRATOR = "ADM"
}

/**
* The user types.
*/
enum class UserType(val code: String) {

    /**
    * A student.
    */
    STUDENT(UserGroup.STUDENT),

    /**
    * A lecturer.
    */
    LECTURER(UserGroup.LECTURER),

    /**
    * A staff member of the faculty secreteriat.
    */
    FACULTY_SECRETARIAT_STAFF(UserGroup.FACULTY_SECRETARIAT_STAFF),

    /**
    * A staff member of the examination authority.
    */
    EXAMINATION_AUTHORITY_STAFF(UserGroup.EXAMINATION_AUTHORITY_STAFF),

    /**
    * An administration staff member.
    */
    ADMINISTRATOR_STAFF(UserGroup.ADMINISTRATOR);

    companion object {

        val valuesByCode = HashMap<String, UserType>()

        init {
            val userTypeValues = UserType.values()

            for (userType in userTypeValues) {
                valuesByCode.put(userType.code, userType);
                valuesByCode.put(userType.name, userType);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: UserType?): UserType? = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): UserType? = tryValueOf(idOrCode, null)
    }
}

@Converter(autoApply = true)
class UserTypeConverter : AttributeConverter<UserType, String> {

    override fun convertToDatabaseColumn(userType: UserType?): String? = if (userType == null) { null } else { userType.code }
    override fun convertToEntityAttribute(code: String?): UserType? = UserType.tryValueOf(code)
}

/**
* A user.
*/
@Entity
@Cacheable
@Table(name = "users",
    uniqueConstraints = [UniqueConstraint(name = "uk_users_email", columnNames = ["email"])],
    indexes = [Index(name = "ix_users_type", columnList = "user_type")],
)
open class User : EntityBase() {

    @Id
    @Column(name = "user_id", nullable = false, updatable = false)
    open var userID: UUID? = null

    @Column(name = "password", nullable = false, length = 60)
    lateinit open var password: String

    /**
    * Email address
    * Maximum length: https://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
    */
    @Column(name = "email", nullable = false, length = 254)
    lateinit open var email: String

    @Convert(converter = UserTypeConverter::class)
    @Column(name = "user_type", columnDefinition = "char(3)", nullable = false)
    lateinit open var userType: UserType

    @Column(name = "admin", nullable = false)
    open var admin: Boolean = false

    @Column(name = "faculty_board_of_examiners", nullable = false)
    open var facultyBoardOfExaminers: Boolean = false

    @ManyToMany(targetEntity = Faculty::class, cascade = [CascadeType.ALL])
    @JoinTable(name = "user_faculties",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "faculty_acronym", referencedColumnName = "faculty_acronym")],
        foreignKey = ForeignKey(name = "fk_user_faculties_u_id"),
        inverseForeignKey = ForeignKey(name = "fk_user_faculties_f_acr"),
    )
    lateinit open var faculties: List<Faculty>

    @ManyToMany(targetEntity = Department::class, cascade = [CascadeType.ALL])
    @JoinTable(name = "user_departments",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "department_acronym", referencedColumnName = "department_acronym")],
        foreignKey = ForeignKey(name = "fk_user_departments_u_id"),
        inverseForeignKey = ForeignKey(name = "fk_user_departments_d_acr"),
    )
    lateinit open var departments: List<Department>

    @OneToMany(targetEntity = Project::class, orphanRemoval = true, mappedBy = "student")
    lateinit open var projects: List<Project>

    @PrePersist
    fun onPrePersist() {
        if (userID == null) {
            userID = UUID.randomUUID()
        }
    }
}

@ApplicationScoped
class UserRepository : RepositoryBase<User, UUID>() {

    fun findByEmailNullable(email: String) = findFirstResultNullable("email", email)

    fun findLecturers(
        pageIndex: Int,
        pageSize: Int,
    ): Multi<User> {
        return findPageStream(
            pageIndex,
            pageSize,
            OrderByEmail,
            "userType = ?1",
            UserType.LECTURER)
    }

    companion object {
        @JvmStatic protected val OrderByEmail = Sort.ascending("email")
    }
}
