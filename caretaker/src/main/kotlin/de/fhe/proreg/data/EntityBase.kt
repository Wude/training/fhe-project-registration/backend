package de.fhe.proreg.data

import de.fhe.proreg.shared.Utils
import java.time.Instant
import jakarta.persistence.Column
import jakarta.persistence.MappedSuperclass
import jakarta.persistence.PrePersist
import jakarta.persistence.PreUpdate

@MappedSuperclass
abstract class EntityBase {

    @Column(name = "creation_instant", nullable = false)
    open var creationInstant: Instant = Instant.ofEpochSecond(0)

    @Column(name = "modification_instant", nullable = false)
    open var modificationInstant: Instant = Instant.ofEpochSecond(0)

    @PrePersist
    fun onPreInsert() {
        if (creationInstant.equals(Utils.EpochStartInstant)) {
            creationInstant = Instant.now()
        }
        modificationInstant = Instant.now()
    }

    @PreUpdate
    fun onPreUpdate() {
        modificationInstant = Instant.now()
    }
}
