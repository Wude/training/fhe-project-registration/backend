@echo off

setlocal EnableDelayedExpansion

set "DB_TYPE=dev"
docker-compose ^
    --env-file .env ^
    -f ./docker/config/gateway.yml ^
    -f ./docker/config/backend-cache.yml ^
    -f ./docker/config/backend-db.yml ^
    -f ./docker/config/db-admin.yml ^
    %*

endlocal
