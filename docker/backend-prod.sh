#!/bin/sh
set -e

(
DB_TYPE=prod \
docker-compose \
    --env-file .env \
    -f ./docker/config/gateway.yml \
    -f ./docker/config/backend-cache.yml \
    -f ./docker/config/backend-db.yml \
    -f ./docker/config/caretaker.yml \
    -f ./docker/config/api.yml \
    $@

    # -f ./docker/config/logger.yml \
    # -f ./docker/config/tracer.yml \
    # -f ./docker/config/monitor.yml \
    # -f ./docker/config/monitor-ui.yml \
)
