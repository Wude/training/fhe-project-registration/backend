@echo off

setlocal EnableDelayedExpansion

set "DB_TYPE=prod"
docker-compose ^
    --env-file .env ^
    -f ./docker/config/gateway.yml ^
    -f ./docker/config/backend-cache.yml ^
    -f ./docker/config/backend-db.yml ^
    -f ./docker/config/caretaker.yml ^
    -f ./docker/config/api.yml ^
    %*

    @REM -f ./docker/config/logger.yml ^
    @REM -f ./docker/config/tracer.yml ^
    @REM -f ./docker/config/monitor.yml ^
    @REM -f ./docker/config/monitor-ui.yml ^

endlocal
