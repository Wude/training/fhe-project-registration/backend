package de.fhe.proreg.resources

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.h2.H2DatabaseTestResource

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource::class)
class UserResourceTest {

    @Test
    fun testHelloEndpoint() {
        given()
          .`when`().get("/api/user/hello")
          .then()
             .statusCode(200)
             .body(`is`("Hello project registration!"))
    }
}
