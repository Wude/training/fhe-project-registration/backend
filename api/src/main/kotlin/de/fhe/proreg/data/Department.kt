package de.fhe.proreg.data

// // import de.fhe.proreg.shared.Utils
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
// import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import java.time.Instant
import java.util.UUID
import jakarta.persistence.Cacheable
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.ForeignKey
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.JoinTable
import jakarta.persistence.ManyToOne
import jakarta.persistence.ManyToMany
import jakarta.persistence.OneToMany
import jakarta.persistence.PrePersist
import jakarta.persistence.Table
import jakarta.persistence.FetchType
import jakarta.enterprise.context.ApplicationScoped
import org.hibernate.annotations.NaturalId

@Entity
@Cacheable
@Table(name = "departments")
open class Department : EntityBase() {

    @Id
    @NaturalId
    @Column(name = "department_acronym", columnDefinition = "char(3)", nullable = false, updatable = false)
    open var acronym: String? = null

    @Column(name = "name", nullable = false)
    lateinit open var name: String

    // @Column(name = "faculty_acronym", nullable = false, insertable = true, updatable = false)
    // lateinit open var facultyAcronym: UUID

    @ManyToOne(optional = false, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinColumn(name = "faculty_acronym", nullable = false, insertable = true, updatable = false,
        foreignKey = ForeignKey(name = "fk_departments_f_acronym"))
    lateinit open var faculty: Faculty

    @OneToMany(targetEntity = Project::class, orphanRemoval = true, mappedBy = "department")
    lateinit open var projects: List<Project>

    @ManyToMany(targetEntity = User::class, cascade = [CascadeType.ALL])
    @JoinTable(name = "user_departments",
        joinColumns = [JoinColumn(name = "department_acronym", referencedColumnName = "department_acronym")],
        inverseJoinColumns = [JoinColumn(name = "user_id", referencedColumnName = "user_id")],
        foreignKey = ForeignKey(name = "fk_user_departments_d_acr"),
        inverseForeignKey = ForeignKey(name = "fk_user_departments_u_id"),
    )
    lateinit open var users: List<User>
}

@ApplicationScoped
class DepartmentRepository : RepositoryBase<Department, String>() { }
