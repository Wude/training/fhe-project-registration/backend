package de.fhe.proreg.data

import io.quarkus.hibernate.orm.panache.PanacheQuery
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase
// import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.quarkus.panache.common.Sort
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.Multi
import java.util.Optional
import jakarta.persistence.LockModeType

abstract class RepositoryBase<Entity, Id> : PanacheRepositoryBase<Entity, Id> {

    fun findByIdNonNullable(id: Id): Uni<Entity> =
        Uni.createFrom().emitter { em ->
            try {
                val value = this.findById(id)
                if (value != null) {
                    em.complete(value)
                } else {
                    em.fail(NullPointerException())
                }
            } catch (t: Throwable) {
                em.fail(t)
            }
        }

    fun findByIdNullableForUpdate(id: Id): Uni<Entity?> =
        Uni.createFrom().emitter { em ->
            em.complete(this.findById(id, LockModeType.PESSIMISTIC_WRITE))
        }

    fun findByIdNullable(id: Id): Uni<Entity?> =
        Uni.createFrom().emitter { em ->
            em.complete(this.findById(id))
        }

    fun findByNullableId(id: Id?): Uni<Entity?> =
        if (id == null ) {
            Uni.createFrom().nullItem()
        } else {
            findByIdNullable(id)
        }

    fun findFirstResultNullable(query: String, vararg params: Any): Uni<Entity?> =
        Uni.createFrom().emitter {
            em -> em.complete(this.find(query, *params).firstResult<Entity?>())
        }

    // fun findStream(query: String, vararg params: Any): Multi<Entity> =
    //     Multi.createFrom().emitter { em ->
    //         val stream = this.find(query, *params).stream<Entity>()
    //         val itor = stream.iterator()
    //         while (itor.hasNext()) {
    //             em.emit(itor.next())
    //         }
    //         em.complete()
    //     }

    fun findPageStream(pageIndex: Int, pageSize: Int, query: String, vararg params: Any): Multi<Entity> =
        Multi.createFrom().emitter { em ->
            val stream = this.find(query, *params)
                .page<Entity>(pageIndex, pageSize)
                .stream<Entity>()
            val itor = stream.iterator()
            while (itor.hasNext()) {
                em.emit(itor.next())
            }
            em.complete()
        }

    fun findPageStream(pageIndex: Int, pageSize: Int, sort: Sort, query: String, vararg params: Any): Multi<Entity> =
        Multi.createFrom().emitter { em ->
            val stream = this.find(query, sort, *params)
                .page<Entity>(pageIndex, pageSize)
                .stream<Entity>()
            val itor = stream.iterator()
            while (itor.hasNext()) {
                em.emit(itor.next())
            }
            em.complete()
        }

    companion object {
        @JvmStatic protected val OrderByCreationInstantAsc = Sort.ascending("creationInstant")
    }
}
