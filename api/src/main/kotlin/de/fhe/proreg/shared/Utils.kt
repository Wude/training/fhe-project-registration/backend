package de.fhe.proreg.shared

import io.smallrye.mutiny.tuples.Tuple2
import io.smallrye.mutiny.tuples.Tuple3
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.IOException
import java.math.BigDecimal
import java.nio.ByteBuffer
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.ParseException
import java.time.Instant
import java.util.Locale
import java.util.UUID
import java.util.Optional
import java.util.Base64
import kotlin.math.min

object Utils {

    @JvmStatic internal val TAG = Utils::class.simpleName
    @JvmStatic val EpochStartInstant = Instant.ofEpochSecond(0)
    @JvmStatic private val decimalFormatSymbols = DecimalFormatSymbols()
    @JvmStatic internal val decimalFormat: DecimalFormat
    @JvmStatic private val germanDecimalFormatSymbols = DecimalFormatSymbols()
    @JvmStatic internal val germanDecimalFormat: DecimalFormat
    @JvmStatic internal val Base64Encoder = Base64.getEncoder()
    @JvmStatic internal val Base64Decoder = Base64.getDecoder()

    init {
        decimalFormatSymbols.groupingSeparator = ','
        decimalFormatSymbols.decimalSeparator = '.'
        decimalFormat = DecimalFormat("#,##0.0#", decimalFormatSymbols)
        decimalFormat.isParseBigDecimal = true
        germanDecimalFormatSymbols.groupingSeparator = '.'
        germanDecimalFormatSymbols.decimalSeparator = ','
        // germanDecimalFormat = NumberFormat.getInstance(Locale.GERMAN) as DecimalFormat
        germanDecimalFormat = DecimalFormat("#,##0.00", germanDecimalFormatSymbols)
        germanDecimalFormat.isParseBigDecimal = true
    }

    /**
    * Try to get an enum value from an id string.
    * 
    * @param <E>          The type of enum to get.
    * @param id           The id string to get an enum value from.
    * @param defaultValue The default enum value.
    * @return An enum value from an id string.
    */
    inline fun <reified E : Enum<E>> tryValueOf(id: String?, defaultValue: E?): E?  {
        if (id != null) {
            return try {
                @Suppress("UPPER_BOUND_VIOLATED")
                enumValueOf<E>(id)
            } catch (e: IllegalArgumentException) {
                defaultValue
            }
        }
        return defaultValue
    }

    /**
        * Divide a long value and ceil the result.
        * 
        * @param dividend The dividend.
        * @param divisor  The divisor.
        * @return The quotient.
        */
    fun divideCeil(dividend: Long, divisor: Long): Long {
        if (divisor != 0L) {
            return dividend / divisor +
                if (dividend % divisor == 0L) {
                    0
                } else {
                    1
                }
        }
        return 0
    }
}

fun <T : Any> Optional<T>.toNullable(): T? = this.orElse(null)

/**
 * Creates a [UUID] from the string standard representation as described in the
 * [#toString] method.
 *
 * @return A [UUID] with the specified value or null when that fails.
 */
fun String?.toUuid(): UUID? {
    return if (this == null) {
        null
    } else {
        try {
            UUID.fromString(this)
        } catch (e: IllegalArgumentException) {
            null
        }
    }
}

/**
 * Parse a [BigDecimal].
 *
 * @return A [BigDecimal].
 */
fun String?.parseBigDecimal(): BigDecimal? =
    if (this == null) {
        null
    } else {
        try {
            Utils.decimalFormat.parse(this) as BigDecimal
        } catch (pe: ParseException) {
            null
        }
    }

/**
 * Parse a [BigDecimal] using german locale.
 *
 * @return A [BigDecimal] using german locale.
 */
fun String?.parseBigDecimalGerman(): BigDecimal? =
    if (this == null) {
        null
    } else {
        try {
            Utils.germanDecimalFormat.parse(this) as BigDecimal
        } catch (pe: ParseException) {
            null
        }
    }

/**
 * Format a [BigDecimal] as [String].
 *
 * @return A [BigDecimal] as [String].
 */
fun BigDecimal?.format(): String? =
    if (this == null) {
        null
    } else {
        Utils.decimalFormat.format(this)
    }

/**
 * Format a [BigDecimal] as [String] using german locale.
 *
 * @return A [BigDecimal] as [String] using german locale.
 */
fun BigDecimal?.formatGerman(): String? =
    if (this == null) {
        null
    } else {
        Utils.germanDecimalFormat.format(this)
    }

fun ByteArray.base64Encode(): String = Utils.Base64Encoder.encodeToString(this)

fun ByteArray?.base64EncodeNullable(): String? =
    if (this == null) { null }
    else { Utils.Base64Encoder.encodeToString(this) }

fun String.base64Decode(): ByteArray = Utils.Base64Decoder.decode(this)

fun String?.base64DecodeNullable(): ByteArray? =
    if (this == null) { null }
    else { Utils.Base64Decoder.decode(this) }

fun InputStream.readAllBytesAndClose(): ByteArray {
    val buffer = ByteArrayOutputStream()
    var readCount: Int = 0
    var byteArray = ByteArray(4)

    try {
        while ({ readCount = this.read(byteArray, 0, byteArray.size); readCount }() != -1) {
            buffer.write(byteArray, 0, readCount)
        }
    } finally { }

    buffer.flush()
    val bytes = buffer.toByteArray()
    buffer.close()
    this.close()
    return bytes
}

operator fun <L, R> Tuple2<L, R>.component1() = this.item1
operator fun <L, R> Tuple2<L, R>.component2() = this.item2

operator fun <T1, T2, T3> Tuple3<T1, T2, T3>.component1() = this.item1
operator fun <T1, T2, T3> Tuple3<T1, T2, T3>.component2() = this.item2
operator fun <T1, T2, T3> Tuple3<T1, T2, T3>.component3() = this.item3
