package de.fhe.proreg

import de.fhe.proreg.services.UserService
import io.smallrye.common.annotation.Blocking
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.infrastructure.Infrastructure
import io.smallrye.jwt.auth.principal.JWTParser
import java.io.IOException
import java.time.Instant
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.context.RequestScoped
import jakarta.enterprise.context.control.RequestContextController
import jakarta.enterprise.inject.Instance
import jakarta.enterprise.inject.spi.CDI
import jakarta.inject.Inject
import jakarta.ws.rs.container.ContainerRequestContext
import jakarta.ws.rs.container.ContainerRequestFilter
import jakarta.ws.rs.container.PreMatching
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.HttpHeaders
import jakarta.ws.rs.ext.Provider
import jakarta.ws.rs.Priorities
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.Claim
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.server.ServerRequestFilter

class JwtBlacklistFilter(
    private val parser: JWTParser,
    @ConfigProperty(name = "quarkus.security.users.file.realm-name")
    private val realm: Instance<String>,
    private val userService: UserService,
) {
    fun unauthorized() = Response.status(Response.Status.UNAUTHORIZED)
        .header(HttpHeaders.WWW_AUTHENTICATE, AUTHORIZATION_SCHEME + " realm=\"" + realm.get() + "\"")
        .build()

    fun filter(requestContext: ContainerRequestContext): Response? {
        try {
            // Injection of a request scoped `JsonWebToken` leads to an error,
            // therefore we get the token seperately by using the header.
            val authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)
            if (authorizationHeader != null) {
                val authorizationHeaderParts = authorizationHeader.split(" ")
                if (authorizationHeaderParts.size == 2 &&
                    authorizationHeaderParts[0].equals(AUTHORIZATION_SCHEME, true))
                {
                    val token = parser.parse(authorizationHeaderParts[1])
                    if (userService.tokenIsInvalid(token))
                    {
                        LOG.warn("User '${token.name}' used blacklisted token")
                        return unauthorized()
                    }
                }
            }
        } finally { }

        // A "null" value is explicitly expected for the filter chain to continue.
        return null
    }

    @ServerRequestFilter
    fun filterUni(requestContext: ContainerRequestContext): Uni<Response> =
        Uni.createFrom().emitter<Response> { em -> em.complete(filter(requestContext)) }
            .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())

    companion object {
        public const val AUTHORIZATION_SCHEME = "Bearer"

        private val LOG: Logger = Logger.getLogger(JwtBlacklistFilter::class.java)
    }
}
