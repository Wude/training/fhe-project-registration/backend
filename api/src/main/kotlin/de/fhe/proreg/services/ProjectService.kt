package de.fhe.proreg.services

import de.fhe.proreg.shared.component1
import de.fhe.proreg.shared.component2
import de.fhe.proreg.shared.component3
import de.fhe.proreg.data.Project
import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.ProjectStatus
import de.fhe.proreg.data.ProjectType
import de.fhe.proreg.data.ProjectDocument
import de.fhe.proreg.data.ProjectDocumentRepository
import de.fhe.proreg.data.User
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.data.UserType
import de.fhe.proreg.data.DepartmentRepository
import de.fhe.proreg.shared.toNullable
import de.fhe.proreg.shared.TimeUtils
import de.fhe.proreg.shared.toGermanDate
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.tuples.Tuple2
import io.vertx.mutiny.redis.client.Response
import java.math.BigDecimal
import java.time.Instant
import java.time.Duration
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.UUID
import jakarta.inject.Inject
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claims
import org.jboss.logging.Logger

/**
* Service for handling projects and project documents
*/
@ApplicationScoped
open class ProjectService(
    val log: Logger,
    val departmentRepository: DepartmentRepository,
    val userRepository: UserRepository,
    val projectRepository: ProjectRepository,
    val projectDocumentRepository: ProjectDocumentRepository,
) {
    /**
    * Creates a new student project.
    * @param studentID The user ID of the student.
    * @param supervisingTutor1ID The user ID of the first lecturer involved.
    * @param supervisingTutor2ID The user ID of the second lecturer involved.
    * @param title The title of the project.
    * @param studentSignature The signature of the student.
    * @return A message indicating an error reason and the ID of a newly created project.
    */
    fun createProject(
        studentID: UUID,
        supervisingTutor1ID: UUID,
        supervisingTutor2ID: UUID?,
        title: String,
        projectType: ProjectType,
        studentSignature: ByteArray,
    ): Uni<Pair<String?, UUID?>> =
        Uni.combine().all().unis(
                userRepository.findByIdNullable(studentID),
                userRepository.findByIdNullable(supervisingTutor1ID),
                userRepository.findByNullableId(supervisingTutor2ID),
            ).asTuple()
            .onItem().transform { (student, supervisingTutor1, supervisingTutor2) ->
                if (student == null) {
                    "The student could not be found" to null
                } else if (supervisingTutor1 == null) {
                    "The first supervising tutor (lecturer) could not be found" to null
                } else if (supervisingTutor1.userType != UserType.LECTURER) {
                    "The user to be first supervising tutor must be a lecturer" to null
                } else {
                    val department = departmentRepository.findByIdNullable("AI").await().indefinitely()

                    if (department == null) {
                        "The department 'AI' could not be found" to null
                    } else {
                        var project = Project()
                        project.faculty = department.faculty
                        project.department = department
                        project.student = student
                        project.supervisingTutor1 = supervisingTutor1
                        if (supervisingTutor2 != null && supervisingTutor2.userType != UserType.LECTURER) {
                            "The user to be first supervising tutor must be a lecturer" to null
                        } else {
                            if (supervisingTutor2 != null) {
                                project.supervisingTutor2 = supervisingTutor2
                            } else {
                                project.supervisingTutor2Accepted = true
                            }

                            project.title = title
                            project.projectType = projectType
                            project.deadline = Instant.now().plus(Duration.of(30, ChronoUnit.DAYS)).toGermanDate()
                            project.studentSignature = studentSignature
                            project.status = ProjectStatus.SUBMITTED
                            projectRepository.persist(project)

                            null to project.projectID
                        }
                    }
                }
            }

    /**
    * View a student project.
    * @param projectID The project ID.
    * @param userID The ID of the user who requests to view the content.
    * @param userGroups The groups to which the user belongs.
    * @return The content of the project with the given project ID.
    */
    fun viewProject(
        projectID: UUID,
        userID: UUID,
        userGroups: Set<String>,
    ): Uni<Pair<String?, Project?>> =
        projectRepository.findByIdNonNullable(projectID)
            .onItem().transform { project ->
                if (project == null) {
                    "A project with the given id does not exist" to null
                } else if (userID.equals(project.student.userID) ||
                    userID.equals(project.supervisingTutor1.userID) ||
                    userID.equals(project.supervisingTutor2?.userID) ||
                    userGroups.contains(UserGroup.FACULTY_BOARD_OF_EXAMINERS) ||
                    userGroups.contains(UserGroup.FACULTY_SECRETARIAT_STAFF) ||
                    userGroups.contains(UserGroup.EXAMINATION_AUTHORITY_STAFF) ||
                    userGroups.contains(UserGroup.ADMINISTRATOR))
                {
                    null to project
                } else {
                    "You may not access this project" to null
                }
            }

    /**
    * List the projects of a student.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param studentID The user ID of the student to list projects for.
    * @return The projects of the student with the given user ID.
    */
    fun viewProjectsByStudent(
        pageIndex: Int,
        pageSize: Int,
        studentID: UUID,
    ): Multi<Project> =
        projectRepository.findByStudentID(pageIndex, pageSize, studentID)

    /**
    * List the projects a lecturer is associated with.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param lecturerID The user ID of the lecturer to list projects for.
    * @return The projects of the lecturer with the given user ID.
    */
    fun viewProjectsByLecturer(
        pageIndex: Int,
        pageSize: Int,
        lecturerID: UUID,
    ): Multi<Project> =
        projectRepository.findByLecturerID(pageIndex, pageSize, lecturerID)

    /**
    * List the projects a user is associated with.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param year The year in which the projects to find were created.
    * @param departmentAcronym The department for which to find projects.
    * @return The projects associated to the user with the given ID.
    */
    fun viewProjectsByYear(
        pageIndex: Int,
        pageSize: Int,
        year: Int,
        departmentAcronym: String?
    ): Multi<Project> =
        if (departmentAcronym != null) {
            projectRepository.findByYearAndDepartment(pageIndex, pageSize, year, departmentAcronym)
        } else {
            projectRepository.findByYear(pageIndex, pageSize, year)
        }

    /**
    * List the projects a user is associated with.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param userID The ID of the user to list projects for.
    * @param userGroups The groups to which the user belongs.
    * @return The projects associated to the user with the given ID.
    */
    fun viewProjectsByUser(
        pageIndex: Int,
        pageSize: Int,
        userID: UUID,
        userGroups: Set<String>,
    ): Multi<Project> =
        if (userGroups.contains(UserGroup.STUDENT)) {
            log.debug("viewProjectsByUser (STUDENT), ${userGroups}")
            viewProjectsByStudent(pageIndex, pageSize, userID)
        } else if (userGroups.contains(UserGroup.LECTURER)) {
            log.debug("viewProjectsByUser (LECTURER), ${userGroups}")
            viewProjectsByLecturer(pageIndex, pageSize, userID)
        } else {
            log.debug("viewProjectsByUser (EMPTY), ${userGroups}")
            Multi.createFrom().items()
        }

    /**
    * List the projects with the given status for a faculty.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param facultyAcronym The faculty to list projects for.
    * @param projectStatus The status of the projects to list.
    * @return The projects with the given status for the given faculty.
    */
    fun viewProjectsByStatus(
        pageIndex: Int,
        pageSize: Int,
        projectStatus: ProjectStatus,
        facultyAcronym: String,
    ): Multi<Project> =
        projectRepository.viewProjectsByStatus(pageIndex, pageSize,
            projectStatus, facultyAcronym)

    /**
    * List the projects with the given status.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param projectStatus The status of the projects to list.
    * @return The projects with the given status.
    */
    fun viewProjectsByStatus(
        pageIndex: Int,
        pageSize: Int,
        projectStatus: ProjectStatus,
    ): Multi<Project> =
        projectRepository.viewProjectsByStatus(pageIndex, pageSize, projectStatus)

    /**
    * List the projects for a faculty.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param facultyAcronym The faculty to list projects for.
    * @return The projects for the given faculty.
    */
    fun viewProjectsByFaculty(
        pageIndex: Int,
        pageSize: Int,
        facultyAcronym: String,
    ): Multi<Project> =
        projectRepository.findByFacultyAcronym(pageIndex, pageSize, facultyAcronym)

    /**
    * List the projects for a department.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @param departmentAcronym The department to list projects for.
    * @return The projects for the given department.
    */
    fun viewProjectsByDepartment(
        pageIndex: Int,
        pageSize: Int,
        departmentAcronym: String,
    ): Multi<Project> =
        projectRepository.findByDepartmentAcronym(pageIndex, pageSize, departmentAcronym)

    /**
    * Create a document for a project.
    * @param projectID The ID of project to create the document for.
    * @param title The title of the document (most likely the filename).
    * @param content The content of the document to create.
    * @return The ID of the newly created project document.
    */
    fun createProjectDocument(
        projectID: UUID,
        title: String,
        content: ByteArray,
    ): Uni<UUID?> =
        projectRepository.findByIdNullable(projectID)
            .onItem().transform { project ->
                if (project == null) {
                    null
                } else {
                    var projectDocument = ProjectDocument()
                    projectDocument.project = project
                    projectDocument.title = title
                    projectDocument.content = content
                    projectDocumentRepository.persist(projectDocument)

                    projectDocument.projectDocumentID
                }
            }

    /**
    * View a student project document.
    * @param projectID The project ID.
    * @param documentID The project document ID.
    * @param userID The ID of the user who requests to view the content.
    * @param userGroups The groups to which the user belongs.
    * @return The content of the project document with the given IDs.
    */
    fun viewProjectDocument(
        projectID: UUID,
        documentID: UUID,
        userID: UUID,
        userGroups: Set<String>,
    ): Uni<Pair<String?, ProjectDocument?>> =
        Uni.combine().all().unis(
            projectRepository.findByIdNullable(projectID),
            projectDocumentRepository.findByIdNullable(documentID),
        ).asTuple().onItem().transform { (project, projectDocument) ->
            if (project == null) {
                "A project with the given id does not exist" to null
            } else if (projectDocument == null) {
                "A project document with the given id does not exist" to null
            } else if (userID.equals(project.student.userID) ||
                userID.equals(project.supervisingTutor1.userID) ||
                userID.equals(project.supervisingTutor2?.userID) ||
                userGroups.contains(UserGroup.FACULTY_BOARD_OF_EXAMINERS) ||
                userGroups.contains(UserGroup.FACULTY_SECRETARIAT_STAFF) ||
                userGroups.contains(UserGroup.EXAMINATION_AUTHORITY_STAFF) ||
                userGroups.contains(UserGroup.ADMINISTRATOR))
            {
                null to projectDocument
            } else {
                "You may not access this project" to null
            }
        }

    /**
    * Accept a student project.
    * @param projectID The project ID.
    * @param supervisingTutorID The user ID of the first/main lecturer.
    * @return Was the student project accepted?
    */
    fun acceptProject(projectID: UUID, supervisingTutorID: UUID): Uni<Boolean> =
        Uni.combine().all().unis(
            projectRepository.findByIdNullableForUpdate(projectID),
            userRepository.findByIdNullable(supervisingTutorID),
        ).asTuple().onItem().transform { (project, supervisingTutor) ->
                if (project == null ||
                    supervisingTutor == null ||
                    project.status == ProjectStatus.ACCEPTED
                ) {
                    false
                } else {
                    if (supervisingTutor.userID!!.equals(
                        project.supervisingTutor1.userID))
                    {
                        project.supervisingTutor1Accepted = true
                    } else if (supervisingTutor.userID!!.equals(
                        project.supervisingTutor2?.userID))
                    {
                        project.supervisingTutor2Accepted = true
                    }
                    if (project.supervisingTutor1Accepted
                        && (project.supervisingTutor2Accepted
                            || project.supervisingTutor2?.userID == null
                        ))
                    {
                        project.status = ProjectStatus.ACCEPTED
                    }
                    projectRepository.persist(project)

                    true
                }
            }

    /**
    * Change a student project status.
    * @param projectID The project ID.
    * @param projectStatus The new status for the project.
    * @return Has the student project status been changed?
    */
    fun changeProjectStatus(
        projectID: UUID,
        projectStatus: ProjectStatus,
    ): Uni<Boolean> =
        projectRepository.findByIdNullableForUpdate(projectID)
            .onItem().transform { project ->
                if (project == null || project.status == projectStatus) {
                    false
                } else {
                    project.status = projectStatus
                    projectRepository.persist(project)

                    true
                }
            }

    /**
    * Grade a student project.
    * @param projectID The project ID.
    * @param grade The grade for the project.
    * @return Has the student project status been graded?
    */
    fun gradeProject(projectID: UUID, grade: BigDecimal): Uni<Boolean> =
        projectRepository.findByIdNullableForUpdate(projectID)
            .onItem().transform { project ->
                if (project == null) {
                    false
                } else {
                    project.grade = grade
                    project.status = ProjectStatus.GRADED
                    projectRepository.persist(project)

                    true
                }
            }
}
