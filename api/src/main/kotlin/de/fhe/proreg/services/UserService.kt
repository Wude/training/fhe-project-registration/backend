package de.fhe.proreg.services

import de.fhe.proreg.data.Department
import de.fhe.proreg.data.DepartmentRepository
import de.fhe.proreg.data.Faculty
import de.fhe.proreg.data.FacultyRepository
import de.fhe.proreg.data.User
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.data.UserType
import de.fhe.proreg.shared.toNullable
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.groups.MultiRepetition
import io.smallrye.mutiny.groups.UniRepeat
import io.quarkus.elytron.security.common.BcryptUtil
import io.quarkus.redis.datasource.ReactiveRedisDataSource
import io.quarkus.redis.datasource.RedisDataSource
import io.quarkus.redis.datasource.keys.KeyCommands
import io.quarkus.redis.datasource.keys.ReactiveKeyCommands
import io.quarkus.redis.datasource.value.ReactiveValueCommands
import io.quarkus.redis.datasource.value.SetArgs
// import io.vertx.mutiny.redis.client.Response
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import java.time.Instant
import java.time.Duration
import jakarta.inject.Inject
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.jboss.logging.Logger
import org.wildfly.security.password.Password
import org.wildfly.security.password.PasswordFactory
import org.wildfly.security.password.interfaces.BCryptPassword
import org.wildfly.security.password.util.ModularCrypt

@ApplicationScoped
open class UserService(
    val log: Logger,
    val redis: ReactiveRedisDataSource,
    val userRepository: UserRepository,
    val facultyRepository: FacultyRepository,
    val departmentRepository: DepartmentRepository,
    @ConfigProperty(name = "de.fhe.proreg.token-validity-seconds", defaultValue = "3600")
    val tokenValiditySeconds: Long,
    @ConfigProperty(name = "mp.jwt.verify.issuer", defaultValue = "https://fh-erfurt.de/proreg")
    val issuer: String,
) {
    val keyCommands: ReactiveKeyCommands<String>
    val valueCommands: ReactiveValueCommands<String, String>
    val redisAwaitDuration = Duration.ofSeconds(60)

    init {
        keyCommands = redis.key()
        valueCommands = redis.value(String::class.java)
    }

    private fun getFaculties(facultyAcronyms: List<String>): Multi<Faculty> =
        Multi.createBy().repeating().uni( { AtomicInteger() }) {
                index -> facultyRepository.findByIdNonNullable(facultyAcronyms[index.getAndIncrement()])
            }
            .atMost(facultyAcronyms.size.toLong())
            .filter { faculty -> faculty != null }

    private fun getDepartments(departmentAcronyms: List<String>): Multi<Department> =
        Multi.createBy().repeating().uni( { AtomicInteger() }) {
                index -> departmentRepository.findByIdNonNullable(departmentAcronyms[index.getAndIncrement()])
            }
            .atMost(departmentAcronyms.size.toLong())
            .filter { department -> department != null }

    private fun User.changePassword(plainPassword: String) {
        this.password = BcryptUtil.bcryptHash(plainPassword)
    }

    private fun User.verifyPassword(plainPassword: String): Boolean =
        try {
            // LOG.debug("Pwd \"${password}\"")
            val rawPassword: Password = ModularCrypt.decode(this.password)
            val factory = PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT)
            val restored = factory.translate(rawPassword) as BCryptPassword
            factory.verify(restored, plainPassword.toCharArray())
        } catch (e: Exception) {
            false
        }

    fun createUser(
        email: String,
        password: String,
        userType: UserType,
        admin: Boolean,
        facultyBoardOfExaminers: Boolean,
        facultyAcronyms: List<String>,
        departmentAcronyms: List<String>
    ): Uni<UUID?> =
        userRepository.findByEmailNullable(email)
            .onItem().transform { existingUser ->
                if (existingUser == null) {
                    val user = User()
                    user.email = email
                    user.changePassword(password)
                    user.userType = userType
                    user.admin = admin
                    user.facultyBoardOfExaminers = facultyBoardOfExaminers
                    user.faculties = getFaculties(facultyAcronyms).collect().asList().await().indefinitely()
                    user.departments = getDepartments(departmentAcronyms).collect().asList().await().indefinitely()
                    userRepository.persist(user)

                    user.userID
                } else {
                    null
                }
            }

    fun changePassword(userID: UUID, password: String): Uni<Boolean> =
        userRepository.findByIdNullableForUpdate(userID)
            .onFailure().recoverWithNull()
            .onItem().transform { user ->
                if (user != null) {
                    user.changePassword(password)
                    userRepository.persist(user)

                    true
                } else {
                    false
                }
            }

    fun getGroups(user: User): Set<String> {
        var groups = mutableSetOf(user.userType.code)
        if (user.admin) { groups.add(UserGroup.ADMINISTRATOR) }
        if (user.facultyBoardOfExaminers) { groups.add(UserGroup.FACULTY_BOARD_OF_EXAMINERS) }
        return groups
    }

    fun createToken(email: String, plainPassword: String): Uni<Pair<String?, User?>> =
        Uni.createFrom().emitter<Pair<String?, User?>> { em ->
            val user = userRepository.findByEmailNullable(email).await().indefinitely()
            val tokenID = UUID.randomUUID()
            if (user == null || user.verifyPassword(plainPassword)) {
                em.complete(null to null)
            } else {
                val token = Jwt.issuer(issuer)
                    .upn(user.email)
                    .groups(getGroups(user))
                    .claim(Claims.sub.name, user.userID)
                    .claim(Claims.email.name, user.email)
                    .claim(Claims.email_verified.name, user.email)
                    .claim(Claims.jti.name, tokenID)
                    .issuedAt(Instant.now())
                    .expiresIn(Duration.ofSeconds(tokenValiditySeconds))
                    .sign()
                em.complete(token to user)
            }
        }

    fun tokenIsInvalid(token: JsonWebToken): Boolean {
        val response = valueCommands.get("token:blacklist:${token.tokenID}").await().atMost(redisAwaitDuration)

        return if (response == null) {
            false
        } else {
            val result = response.toString()
            result.equals("OK")
        }
    }

    fun revokeToken(token: JsonWebToken): Uni<Void> =
        valueCommands.set("token:blacklist:${token.tokenID}", "OK", SetArgs().ex(Duration.ofMinutes(tokenValiditySeconds))).replaceWithVoid()

    /**
    * List lecturers.
    * @param pageIndex The index of the result page to view.
    * @param pageSize The size of the result page to view.
    * @return The lecturers.
    */
    fun viewLecturers(
        pageIndex: Int,
        pageSize: Int,
    ): Multi<User> =
        userRepository.findLecturers(pageIndex, pageSize)
}
