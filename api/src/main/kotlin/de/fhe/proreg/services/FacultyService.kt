package de.fhe.proreg.services

import de.fhe.proreg.shared.component1
import de.fhe.proreg.shared.component2
import de.fhe.proreg.data.Department
import de.fhe.proreg.data.DepartmentRepository
import de.fhe.proreg.data.Faculty
import de.fhe.proreg.data.FacultyRepository
import de.fhe.proreg.shared.toNullable
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.redis.client.Response
import java.time.Instant
import java.time.Duration
import jakarta.inject.Inject
import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claims
import org.jboss.logging.Logger

/**
* Service for handling faculties and departments
*/
@ApplicationScoped
open class FacultyService(
    val log: Logger,
    val facultyRepository: FacultyRepository,
    val departmentRepository: DepartmentRepository,
) {
    fun createFaculty(facultyAcronym: String, facultyName: String): Uni<String?> =
        facultyRepository.findByIdNullable(facultyAcronym)
            .onItem().transform { faculty ->
                if (faculty != null) {
                    "A faculty with the given acronym '${facultyAcronym}' already exists"
                } else {
                    var newFaculty = Faculty()
                    newFaculty.acronym = facultyAcronym
                    newFaculty.name = facultyName
                    facultyRepository.persist(newFaculty)

                    null
                }
            }

    fun createDepartment(
        facultyAcronym: String,
        departmentAcronym: String,
        departmentName: String,
    ): Uni<String?> =
        Uni.combine().all().unis(
            facultyRepository.findByIdNullable(facultyAcronym),
            departmentRepository.findByIdNullable(departmentAcronym)
        ).asTuple().onItem().transform { (faculty, department) ->
                if (faculty == null) {
                    "A faculty with the given acronym '${facultyAcronym}' already exists"
                } else if (department != null) {
                    "A department with the given acronym '${departmentAcronym}' already exists"
                } else {
                    var newDepartment = Department()
                    newDepartment.acronym = departmentAcronym
                    newDepartment.name = departmentName
                    newDepartment.faculty = faculty!!
                    departmentRepository.persist(newDepartment)

                    null
                }
            }
}
