package de.fhe.proreg.models

import de.fhe.proreg.data.ProjectStatus
import de.fhe.proreg.data.ProjectType
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.UUID

data class ProjectCreationRequest(
    var studentID: UUID,
    var supervisingTutor1ID: UUID,
    var supervisingTutor2ID: UUID?,
    var title: String,
    var projectType: String,
    var studentSignature: String,
    // var departmentAcronym: String,
)
data class ProjectCreationResponse(
    var projectID: UUID,
)

data class ProjectViewResponse(
    var projectID: UUID,
    var projectType: String,
    var studentID: UUID,
    var studentEmail: String,
    var creationInstant: Instant,
    var modificationInstant: Instant,
    var supervisingTutor1ID: UUID,
    var supervisingTutor1Email: String,
    var supervisingTutor2ID: UUID?,
    var supervisingTutor2Email: String?,
    var title: String,
    var grade: BigDecimal?,
    var deadline: LocalDate?,
    var status: String,
    var supervisingTutor1Accepted: Boolean,
    var supervisingTutor2Accepted: Boolean,
    var studentSignature: String,
    var supervisingTutor1Signature: String?,
    var supervisingTutor2Signature: String?,
)

data class ProjectListResponse(
    var projects: List<ProjectViewResponse>
)
