package de.fhe.proreg.models

data class DepartmentCreationRequest(
    var facultyAcronym: String,
    var departmentAcronym: String,
    var departmentName: String
)

data class DepartmentViewResponse(
    var facultyAcronym: String,
    var departmentAcronym: String,
    var departmentName: String
)
