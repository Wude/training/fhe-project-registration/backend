package de.fhe.proreg.models

data class BaseResponse(
    var status: Int,
    var reason: String
)
