package de.fhe.proreg.models

import de.fhe.proreg.data.UserType
import java.util.UUID

data class UserLoginRequest(
    var email: String,
    var password: String
)

data class UserLoginResponse(
    var accessToken: String,
    var user: UserViewResponse,
)

data class UserCreationRequest(
    var email: String,
    var password: String,
    var userType: UserType,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    var facultyAcronyms: List<String>,
    var departmentAcronyms: List<String>,
)

data class UserCreationResponse(
    var userID: UUID,
)

data class UserPasswordChangeRequest(
    var password: String,
)

data class UserViewResponse(
    var userID: UUID,
    var email: String,
    var userType: String,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    var facultyAcronyms: List<String>,
    var departmentAcronyms: List<String>,
)

data class UserListResponse(
    var users: List<UserViewResponse>
)
