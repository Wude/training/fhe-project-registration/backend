package de.fhe.proreg.models

data class FacultyCreationRequest(
    var facultyAcronym: String,
    var facultyName: String
)

data class FacultyViewResponse(
    var facultyAcronym: String,
    var facultyName: String
)
