package de.fhe.proreg.models

import java.io.InputStream
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.UUID
import jakarta.ws.rs.FormParam
import jakarta.ws.rs.core.MediaType
// import org.jboss.resteasy.annotations.providers.multipart.PartType
import org.jboss.resteasy.reactive.PartType

data class ProjectDocumentCreationRequest(

    @FormParam("title")
    @PartType(MediaType.TEXT_PLAIN)
    var title: String,

    @FormParam("content")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    var content: InputStream
)

data class ProjectDocumentViewResponse(
    var projectID: UUID,
    var projectDocumentID: UUID,
    var creationInstant: Instant,
    var modificationInstant: Instant,
    var title: String,
    var content: ByteArray,
)
