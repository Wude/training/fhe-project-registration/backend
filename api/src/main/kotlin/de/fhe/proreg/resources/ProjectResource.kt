package de.fhe.proreg.resources

// import de.fhe.proreg.shared.Utils
import de.fhe.proreg.RestStatusInt
import de.fhe.proreg.RestStatusString
import de.fhe.proreg.data.Project
import de.fhe.proreg.data.ProjectStatus
import de.fhe.proreg.data.ProjectType
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserType
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.data.ProjectDocument
import de.fhe.proreg.models.BaseResponse
import de.fhe.proreg.models.ProjectViewResponse
import de.fhe.proreg.models.ProjectListResponse
import de.fhe.proreg.models.ProjectCreationRequest
import de.fhe.proreg.models.ProjectCreationResponse
import de.fhe.proreg.models.ProjectDocumentCreationRequest
import de.fhe.proreg.models.ProjectDocumentViewResponse
import de.fhe.proreg.services.ProjectService
import de.fhe.proreg.services.UserService
import de.fhe.proreg.shared.base64Encode
import de.fhe.proreg.shared.base64EncodeNullable
import de.fhe.proreg.shared.base64Decode
import de.fhe.proreg.shared.base64DecodeNullable
import de.fhe.proreg.shared.readAllBytesAndClose
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.Multi
// import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional
import java.io.IOException
import java.io.InputStream
import java.io.ByteArrayOutputStream
import java.math.BigDecimal
import java.util.Base64
import java.util.UUID
import jakarta.annotation.security.PermitAll
import jakarta.annotation.security.RolesAllowed
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.context.RequestScoped
import jakarta.inject.Inject
import jakarta.transaction.Transactional
import jakarta.transaction.Transactional as ReactiveTransactional
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.FormParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.QueryParam
import jakarta.ws.rs.core.Context
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.BadRequestException
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claim
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Gauge
import org.eclipse.microprofile.metrics.annotation.Timed
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.PartType
import org.jboss.resteasy.reactive.server.multipart.MultipartFormDataOutput
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse

@Path("/api/project")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
open class ProjectResource(
    val log: Logger,
    val token: JsonWebToken,
    val userService: UserService,
    val projectService: ProjectService,
) : ResourceBase() {

    @POST
    @ReactiveTransactional
    @RolesAllowed(UserGroup.STUDENT)
    @Path("/create")
    @Counted(name = "projectCreateCount",
        description = "Project create count")
    @Timed(name = "projectCreateTime",
        description = "Project create handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a new project",
        description = "Create a new project with the given contents")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = "Project contents invalid",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a student may create a new project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createProject(
        request: ProjectCreationRequest
    ): Uni<RestResponse<ProjectCreationResponse>> =
        projectService.createProject(
            request.studentID,
            request.supervisingTutor1ID,
            request.supervisingTutor2ID,
            request.title,
            ProjectType.tryValueOf(request.projectType)!!,
            request.studentSignature.base64Decode())
        .onItem().transform { (reasonPhrase, projectID) ->
            if (projectID != null && reasonPhrase == null) {
                log.debug("User '${token.name}' created a project ${projectID}")
                createRestResponse(ProjectCreationResponse(projectID), RestStatusInt.OK_200, PROJECT_CREATED)
            } else {
                createRestResponse(RestStatusInt.BAD_REQUEST_400, reasonPhrase!!)
            }
        }

    @GET
    @ReactiveTransactional
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/{projectID}")
    @Counted(name = "projectViewCount",
        description = "Project view count")
    @Timed(name = "projectViewTime",
        description = "Project view handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "View a project",
        description = "View the project with the given id")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT,
                implementation = ProjectViewResponse::class))]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a valid user may view this project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun viewProject(
        @PathParam("projectID") projectID: UUID,
    ): Uni<RestResponse<ProjectViewResponse>> =
        projectService.viewProject(projectID, UUID.fromString(token.subject), token.groups)
            .onItem().transform { (reasonPhrase, project) ->
                if (project != null) { createRestResponse(toData(project), RestStatusInt.OK_200, PROJECT_FOUND) }
                else { createRestResponse(RestStatusInt.BAD_REQUEST_400, reasonPhrase ?: PROJECT_NOT_EXISTS) }
            }

    @GET
    @ReactiveTransactional
    @RolesAllowed(UserGroup.STUDENT, UserGroup.LECTURER)
    @Path("/year/{year}/paged/{pageIndex}/{pageSize}")
    @Counted(name = "projectListCount",
        description = "Project list count")
    @Timed(name = "projectListTime",
        description = "Project list handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List projects",
        description = "List projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT,
                implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a student or lecturer may list projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listProjectsByYear(
        @RestPath pageIndex: Int,
        @RestPath pageSize: Int,
        @RestPath year: Int,
        @QueryParam("dept") departmentAcronym: String?,
    ): Multi<ProjectViewResponse> =
        projectService.viewProjectsByYear(pageIndex, pageSize, year, departmentAcronym)
            .onItem().transform<ProjectViewResponse> { item -> toData(item) }

    @GET
    @ReactiveTransactional
    @RolesAllowed(UserGroup.STUDENT, UserGroup.LECTURER)
    @Path("/related/paged/{pageIndex}/{pageSize}")
    @Counted(name = "projectListRelatedCount",
        description = "Project list related count")
    @Timed(name = "projectListRelatedTime",
        description = "Project list related handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List related projects",
        description = "List related projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT,
                implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a student or lecturer may list related projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listRelatedProjects(
        @RestPath pageIndex: Int,
        @RestPath pageSize: Int,
    ): Multi<ProjectViewResponse> =
        projectService.viewProjectsByUser(pageIndex, pageSize, UUID.fromString(token.subject), token.groups)
            .onItem().transform<ProjectViewResponse> { item -> toData(item) }

    @GET
    @ReactiveTransactional
    @RolesAllowed(UserGroup.FACULTY_SECRETARIAT_STAFF)
    @Path("/accepted/faculty/{facultyAcronym}/paged/{pageIndex}/{pageSize}")
    @Counted(name = "projectListAcceptedCount",
        description = "Project list accepted count")
    @Timed(name = "projectListAcceptedTime",
        description = "Project list accepted handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List accepted projects",
        description = "List accepted projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.NO_CONTENT_204,
            description = PROJECT_NOT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a secreteriat staff member may list accepted projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listAcceptedProjects(
        @RestPath pageIndex: Int,
        @RestPath pageSize: Int,
        @PathParam("facultyAcronym") facultyAcronym: String
    ): Multi<ProjectViewResponse> =
        projectService.viewProjectsByStatus(pageIndex, pageSize, ProjectStatus.ACCEPTED, facultyAcronym)
            .onItem().transform<ProjectViewResponse> { item -> toData(item) }

    @GET
    @ReactiveTransactional
    @RolesAllowed(UserGroup.FACULTY_BOARD_OF_EXAMINERS)
    @Path("/deadline-extension-requested/faculty/{facultyAcronym}/paged/{pageIndex}/{pageSize}")
    @Counted(name = "projectListDeadlineExtensionRequestedCount", description = "Project list deadline extension requested count")
    @Timed(name = "projectListDeadlineExtensionRequestedTime", description = "Project list deadline extension requested handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List deadline extension requested projects",
        description = "List deadline extension requested projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.NO_CONTENT_204, description = PROJECT_NOT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a member of the board of examiners may list projects for which a deadline extension was requested",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listDeadlineExtensionRequestedProjects(
        @RestPath pageIndex: Int,
        @RestPath pageSize: Int,
        @PathParam("facultyAcronym") facultyAcronym: String
    ): Multi<ProjectViewResponse> =
        projectService.viewProjectsByStatus(pageIndex, pageSize, ProjectStatus.DEADLINE_EXTENSION_REQUESTED, facultyAcronym)
            .onItem().transform<ProjectViewResponse> { item -> toData(item) }

    @GET
    @ReactiveTransactional
    @RolesAllowed(UserGroup.EXAMINATION_AUTHORITY_STAFF)
    @Path("/graded/paged/{pageIndex}/{pageSize}")
    @Counted(name = "projectListGradedRequestedCount", description = "Project list graded count")
    @Timed(name = "projectListGradedRequestedTime", description = "Project list graded handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List graded projects",
        description = "List graded projects")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT, implementation = ProjectListResponse::class))]),
        APIResponse(responseCode = RestStatusString.NO_CONTENT_204, description = PROJECT_NOT_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a member of the board of examiners may list accepted projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listGradedProjects(
        @RestPath pageIndex: Int,
        @RestPath pageSize: Int,
    ): Multi<ProjectViewResponse> =
        projectService.viewProjectsByStatus(pageIndex, pageSize, ProjectStatus.GRADED)
            .onItem().transform<ProjectViewResponse> { item -> toData(item) }

    @PUT
    @ReactiveTransactional
    @RolesAllowed(UserGroup.LECTURER)
    @Path("/{projectID}/reject")
    @Counted(name = "projectRejectCount", description = "Project reject count")
    @Timed(name = "projectRejectTime", description = "Project reject handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Reject a project",
        description = "Reject a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_REJECTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401, description = "Only a lecturer may reject a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun rejectProject(@PathParam("projectID") projectID: UUID): Uni<Response> =
        projectService.changeProjectStatus(projectID, ProjectStatus.REJECTED)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' rejected project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_REJECTED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.LECTURER)
    @Path("/{projectID}/accept")
    @Counted(name = "projectAcceptCount",
        description = "Project accept count")
    @Timed(name = "projectAcceptTime",
        description = "Project accept handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Accept a project",
        description = "Accept a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_ACCEPTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a lecturer may accept a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun acceptProject(@PathParam("projectID") projectID: UUID): Uni<Response> =
        // projectService.changeProjectStatus(projectID, ProjectStatus.ACCEPTED)
        projectService.acceptProject(projectID, UUID.fromString(token.subject))
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' accepted project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_ACCEPTED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_SECRETARIAT_STAFF)
    @Path("/{projectID}/register")
    @Counted(name = "projectRegisterCount",
        description = "Project register count")
    @Timed(name = "projectRegisterTime",
        description = "Project register handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Register a project",
        description = "Register a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_REGISTERED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the secreteriat staff may register a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun registerProject(@PathParam("projectID") projectID: UUID): Uni<Response> =
        projectService.changeProjectStatus(projectID, ProjectStatus.REGISTERED)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' registered project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_REGISTERED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @POST
    @Transactional
    @RolesAllowed(UserGroup.STUDENT)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/{projectID}/document/create")
    @Counted(name = "projectDocumentCreateCount",
        description = "Project document create count")
    @Timed(name = "projectDocumentCreateTime",
        description = "Project document create handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a new project document",
        description = "Create a new project document")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_REGISTERED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the secreteriat staff may register a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createProjectDocument(
        @PathParam("projectID") projectID: UUID,
        request: ProjectDocumentCreationRequest,
    ): Uni<Response> =
        projectService.createProjectDocument(projectID, request.title, request.content.readAllBytesAndClose())
            .onItem().transform { documentID ->
                if (documentID != null ) {
                    log.debug("User '${token.name}' create document {documentID} for project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_REGISTERED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @GET
    @ReactiveTransactional
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER)
    // @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Produces("multipart/mixed")
    @Path("/{projectID}/document/{documentID}")
    @Counted(name = "projectDocumentCreateCount",
        description = "Project document create count")
    @Timed(name = "projectDocumentCreateTime",
        description = "Project document create handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "View a project document",
        description = "View the project document with the given id")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_REGISTERED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the secreteriat staff may register a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun viewProjectDocument(
        @PathParam("projectID") projectID: UUID,
        @PathParam("documentID") documentID: UUID,
    ): Uni<Response> =
        projectService.viewProjectDocument(projectID, documentID, UUID.fromString(token.subject), token.groups)
            .onItem().transform { (reasonPhrase, projectDocument) ->
                if (projectDocument != null) {
                    val output = MultipartFormDataOutput()
                    output.addFormData("metadata", toData(projectDocument), MediaType.APPLICATION_JSON_TYPE)
                    output.addFormData("content", projectDocument.content, MediaType.APPLICATION_OCTET_STREAM_TYPE)

                    createResponse(output, RestStatusInt.OK_200, PROJECT_DOCUMENT_FOUND)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, reasonPhrase ?: PROJECT_DOCUMENT_NOT_EXISTS)
                }
            }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.STUDENT)
    @Path("/{projectID}/deadline-extension/request")
    @Counted(name = "projectDeadlineExtensionRequestCount", description = "Project deadline extension request count")
    @Timed(name = "projectDeadlineExtensionRequestTime", description = "Project deadline extension request handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Request a deadline extension for a project",
        description = "Request the deadline extension for a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_DEADLINE_EXTENSION_REQUESTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a student may request a deadline extension for a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun requestDeadlineExtension(@PathParam("projectID") projectID: UUID): Uni<Response> =
        projectService.changeProjectStatus(projectID, ProjectStatus.DEADLINE_EXTENSION_REQUESTED)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' requested deadline extension for project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_DEADLINE_EXTENSION_REQUESTED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_BOARD_OF_EXAMINERS)
    @Path("/{projectID}/deadline-extension/reject")
    @Counted(name = "projectDeadlineExtensionRejectCount", description = "Project deadline extension reject count")
    @Timed(name = "projectDeadlineExtensionRejectTime", description = "Project deadline extension reject handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Reject the deadline extension of a project",
        description = "Reject the deadline extension of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_DEADLINE_EXTENSION_REJECTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a member of the board of examiners may reject the deadline extension of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun rejectDeadlineExtension(@PathParam("projectID") projectID: UUID): Uni<Response> =
        projectService.changeProjectStatus(projectID, ProjectStatus.DEADLINE_EXTENSION_REJECTED)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' rejected deadline extension for project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_DEADLINE_EXTENSION_REJECTED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @PUT
    @Transactional
    @RolesAllowed(UserGroup.FACULTY_BOARD_OF_EXAMINERS)
    @Path("/{projectID}/deadline-extension/grant")
    @Counted(name = "projectDeadlineExtensionGrantCount", description = "Project deadline extension grant count")
    @Timed(name = "projectDeadlineExtensionGrantTime", description = "Project deadline extension grant handling time", unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Grant the deadline extension of a project",
        description = "Grant the deadline extension of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200, description = PROJECT_DEADLINE_EXTENSION_GRANTED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400, description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a member of the board of examiners may grant the deadline extension of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun grantDeadlineExtension(@PathParam("projectID") projectID: UUID): Uni<Response> =
        projectService.changeProjectStatus(projectID, ProjectStatus.DEADLINE_EXTENSION_GRANTED)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' granted deadline extension for project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_DEADLINE_EXTENSION_GRANTED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @PUT
    @ReactiveTransactional
    @RolesAllowed(UserGroup.LECTURER)
    @Path("/{projectID}/grade/{grade}")
    @Counted(name = "projectGradeCount",
        description = "Project grade count")
    @Timed(name = "projectGradeTime",
        description = "Project grade handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Grade of a project",
        description = "Grade of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_GRADING_RECORDED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the examination authority may record the grading of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun gradeProject(
        @PathParam("projectID") projectID: UUID,
        @PathParam("grade") grade: BigDecimal,
    ): Uni<Response> =
        projectService.gradeProject(projectID, grade)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' graded project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_GRADED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    @PUT
    @ReactiveTransactional
    @RolesAllowed(UserGroup.EXAMINATION_AUTHORITY_STAFF)
    @Path("/{projectID}/record-grading")
    @Counted(name = "projectRecordGradingCount",
        description = "Project record grading count")
    @Timed(name = "projectRecordGradingTime",
        description = "Project record grading handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Record the grading of a project",
        description = "Record the grading of a project")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = PROJECT_GRADING_RECORDED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = PROJECT_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only the examination authority may record the grading of a project",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun recordGrading(@PathParam("projectID") projectID: UUID): Uni<Response> =
        projectService.changeProjectStatus(projectID, ProjectStatus.COMPLETED)
            .onItem().transform { success ->
                if (success) {
                    log.debug("User '${token.name}' grading recorded for project {projectID}")
                    createResponse(RestStatusInt.OK_200, PROJECT_GRADING_RECORDED)
                } else {
                    createResponse(RestStatusInt.BAD_REQUEST_400, PROJECT_NOT_EXISTS)
                }
            }

    companion object {

        private fun toData(projects: List<Project>): ProjectListResponse {
            val projectViews = mutableListOf<ProjectViewResponse>()
            for (project in projects) {
                projectViews.add(toData(project))
            }
            return ProjectListResponse(projectViews)
        }

        private fun toData(project: Project) = ProjectViewResponse(
            project.projectID!!,
            project.projectType.code,
            project.student.userID!!,
            project.student.email,
            project.creationInstant,
            project.modificationInstant,
            project.supervisingTutor1.userID!!,
            project.supervisingTutor1.email,
            project.supervisingTutor2?.userID,
            project.supervisingTutor2?.email,
            project.title,
            project.grade,
            project.deadline,
            project.status.code,
            project.supervisingTutor1Accepted,
            project.supervisingTutor2Accepted,
            project.studentSignature.base64Encode(),
            project.supervisingTutor1Signature.base64EncodeNullable(),
            project.supervisingTutor2Signature.base64EncodeNullable())

        private fun toData(projectDocument: ProjectDocument) = ProjectDocumentViewResponse(
            projectDocument.project.projectID!!,
            projectDocument.projectDocumentID!!,
            projectDocument.creationInstant,
            projectDocument.modificationInstant,
            projectDocument.title,
            projectDocument.content)

        private const val PROJECT_NOT_EXISTS = "A project with the given id does not exist"
        private const val PROJECT_DOCUMENT_NOT_EXISTS = "A project document with the given id does not exist"
        private const val PROJECT_NOT_FOUND = "No project found"
        private const val PROJECT_DOCUMENT_NOT_FOUND = "No project document found"
        private const val PROJECT_FOUND = "Project found"
        private const val PROJECT_DOCUMENT_FOUND = "Project document found"
        private const val PROJECT_CREATED = "Project created"
        private const val PROJECT_ACCEPTED = "Project accepted"
        private const val PROJECT_REJECTED = "Project rejected"
        private const val PROJECT_REGISTERED = "Project registered"
        private const val PROJECT_GRADED = "Project graded"
        private const val PROJECT_GRADING_RECORDED = "Project grading recorded"
        private const val PROJECT_DEADLINE_EXTENSION_REQUESTED = "Project deadline extension requested"
        private const val PROJECT_DEADLINE_EXTENSION_GRANTED = "Project deadline extension granted"
        private const val PROJECT_DEADLINE_EXTENSION_REJECTED = "Project deadline extension rejected"
    }
}
