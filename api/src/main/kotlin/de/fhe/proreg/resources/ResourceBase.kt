package de.fhe.proreg.resources

import de.fhe.proreg.RestStatusInt
import de.fhe.proreg.models.BaseResponse
import io.smallrye.mutiny.Uni
import java.io.IOException
import java.io.InputStream
import java.io.ByteArrayOutputStream
import java.util.function.BiFunction
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.ResponseBuilder
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.RestResponse.ResponseBuilder as RestResponseBuilder
// import org.jboss.resteasy.reactive.RestResponse.Status
import okhttp3.ResponseBody

abstract class ResourceBase {

    protected fun createResponse(statusCode: Int, reasonPhrase: String) : Response =
        Response.ok(BaseResponse(statusCode, reasonPhrase)).status(statusCode, reasonPhrase).build()

    protected fun createResponse(statusCode: Int) : Response =
        Response.status(statusCode).build()

    protected fun <T> createRestResponse(statusCode: Int, reasonPhrase: String) : RestResponse<T> =
        RestResponseBuilder.create<T>(statusCode, reasonPhrase).build()

    protected fun <T> createRestResponse(content: T, statusCode: Int, reasonPhrase: String) : RestResponse<T> =
        RestResponseBuilder.create<T>(statusCode, reasonPhrase).entity(content).build()

    protected fun <T> createResponse(content: T, statusCode: Int, reasonPhrase: String) : Response =
        // ResponseBody.ok(content).status(statusCode, reasonPhrase).build()
        // ResponseBuilder.status(statusCode, reasonPhrase).entity(content).build()
        Response.status(statusCode, reasonPhrase).entity(content).build()

    protected fun <T> createResponse(content: T, statusCode: Int) : Response =
        Response.status(statusCode).entity(content).build()

    // // protected fun <T> createResponse(content: T, statusCode: Int = RestStatusInt.OK_200) : RestResponse<T> =
    // protected fun <T> createResponse(content: T, statusCode: Int = RestStatusInt.OK_200) : Response =
    //     // ResponseBuilder.create(Status.fromStatusCode(statusCode), content).build()
    //     // ResponseBuilder.status(statusCode).entity(content).build()
    //     Response.status(statusCode).entity(content).build()

    // protected fun createResponse(uni: Uni<Void>, statusCode: Int, reasonPhrase: String) : Uni<Response> =
    //     // Response.ok(BaseResponse(statusCode, reasonPhrase)).status(statusCode, reasonPhrase).build()
    //     uni.onFailure().recoverWithNull().onItem().transform { _ -> Response.status(statusCode, reasonPhrase).build()}

    // protected fun createResponse(uni: Uni<Void>, statusCode: Int) : Uni<Response> =
    //     // Response.status(statusCode).build()
    //     uni.onFailure().recoverWithNull().onItem().transform { _ -> Response.status(statusCode).build()}

    // // protected fun <T> createRestResponse(content: Uni<T>, statusCode: Int, reasonPhrase: String) : Uni<RestResponse<T>> =
    // protected fun <T> createRestResponse(content: Uni<T>, statusCode: Int, reasonPhrase: String) : Uni<Response> =
    //     // content.onItemOrFailure().transform {
    //     //     item, error -> ResponseBuilder.status(statusCode, reasonPhrase).entity(item).build()
    //     // }
    //     content.onFailure().recoverWithNull()
    //         .onItem().transform { item ->
    //             if (item != null) {
    //                 // RestResponseBuilder.create(statusCode, reasonPhrase).entity(item).build()
    //                 Response.status(statusCode, reasonPhrase).entity(item).build()
    //             } else {
    //                 // RestResponseBuilder.create(statusCode, reasonPhrase).build()
    //                 Response.status(statusCode, reasonPhrase).build()
    //             }
    //         }
    //     // ResponseBody.ok(content).status(statusCode, reasonPhrase).build()
    //     // ResponseBuilder.status(statusCode, reasonPhrase).entity(content).build()

    // // protected fun <T> createRestResponse(content: Uni<T>, statusCode: Int = RestStatusInt.OK_200) : Uni<RestResponse<T>> =
    // protected fun <T> createRestResponse(content: Uni<T>, statusCode: Int = RestStatusInt.OK_200) : Uni<Response> =
    //     // ResponseBuilder.create(Status.fromStatusCode(statusCode), content).build()
    //     // RestResponseBuilder.status(statusCode).entity(content).build()
    //     content.onFailure().recoverWithNull()
    //         // .onItem().transform { item -> RestResponseBuilder.create(statusCode).entity(item).build() }
    //         .onItem().transform { item -> Response.status(statusCode).entity(item).build() }

    // // // inline fun <reified T> checkType() = when (T::class) {
    // // //     TypeA::class -> println("TypeA")
    // // //     else -> println("Type not recognized")
    // // // }

    protected inline fun <reified T> createUniResponse(
        content: Uni<T>,
        successStatusCode: Int = RestStatusInt.OK_200,
        successReasonPhrase: String = "",
    ) : Uni<Response> {
        var statusCode = successStatusCode
        return content.onFailure().invoke { _ -> statusCode = RestStatusInt.BAD_REQUEST_400 }
            .onFailure().recoverWithNull().onItem().transform { item: T ->
            if (item == null && T::class != Void::class) {
                statusCode = RestStatusInt.NO_CONTENT_204
            }
            if (item != null && T::class != Void::class) {
                Response.status(statusCode, successReasonPhrase).entity(item).build()
            } else {
                Response.status(statusCode, successReasonPhrase).build()
            }
        }
    }
}
