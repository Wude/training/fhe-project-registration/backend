package de.fhe.proreg.resources

import de.fhe.proreg.RestStatusInt
import de.fhe.proreg.RestStatusString
import de.fhe.proreg.data.User
import de.fhe.proreg.data.UserGroup
import de.fhe.proreg.data.UserType
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.models.UserCreationRequest
import de.fhe.proreg.models.UserCreationResponse
import de.fhe.proreg.models.UserLoginRequest
import de.fhe.proreg.models.UserLoginResponse
import de.fhe.proreg.models.UserListResponse
import de.fhe.proreg.models.UserViewResponse
import de.fhe.proreg.models.UserPasswordChangeRequest
import de.fhe.proreg.services.UserService
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.Multi
import java.util.UUID
import jakarta.annotation.security.PermitAll
import jakarta.annotation.security.RolesAllowed
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.context.RequestScoped
import jakarta.inject.Inject
import jakarta.transaction.Transactional
import jakarta.transaction.Transactional as ReactiveTransactional
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.core.Context
import jakarta.ws.rs.core.SecurityContext
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.jwt.Claim
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Gauge
import org.eclipse.microprofile.metrics.annotation.Timed
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.RestPath

@Path("/api/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
open class UserResource(
    val log: Logger,
    val token: JsonWebToken?,
    val userService: UserService,
) : ResourceBase() {

    @POST
    @ReactiveTransactional
    @RolesAllowed(UserGroup.ADMINISTRATOR)
    @Path("/create")
    @Counted(name = "userCreateCount", description = "User create count")
    @Timed(name = "userCreateTime", description = "User create handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Create a new user",
        description = "Create a new user for the project registration")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.CREATED_201,
            description = USER_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = USER_ALREADY_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun createUser(request: UserCreationRequest): Uni<Response> =
        userService.createUser(
                request.email,
                request.password,
                request.userType,
                request.admin,
                request.facultyBoardOfExaminers,
                request.facultyAcronyms,
                request.departmentAcronyms)
            .onItem().transform { userID ->
                if (userID != null) {
                    log.debug("User '${request.email}' created")
                    createResponse(UserCreationResponse(userID), RestStatusInt.CREATED_201, USER_CREATED)
                } else {
                    log.warn("User '${request.email}' already exists")
                    createResponse(RestStatusInt.BAD_REQUEST_400)
                }
            }

    @PUT
    @ReactiveTransactional
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/changePassword")
    @Counted(name = "userChangePasswordCount",
        description = "User change password count")
    @Timed(name = "userChangePasswordTime",
        description = "User change password handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Change password",
        description = "Change the password of the calling user")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.CREATED_201,
            description = USER_CREATED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = USER_ALREADY_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "A non-user cannot change password",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun changePassword(request: UserPasswordChangeRequest): Uni<Response> {
        val userID = UUID.fromString(token!!.subject)
        val email = token!!.name

        return userService.changePassword(userID, request.password)
            .onFailure().recoverWithNull()
            .onItem().transform {
                success ->
                if (success) {
                    log.info("User '${email}' changed password")
                    createResponse(RestStatusInt.CREATED_201, USER_CREATED)
                } else {
                    log.warn("User '${email}' password change attempt failed")
                    createResponse(RestStatusInt.BAD_REQUEST_400)
                }
            }
    }

    @POST
    @ReactiveTransactional
    @PermitAll
    @Path("/login")
    @Counted(name = "loginCount",
        description = "Login attempt count")
    @Timed(name = "loginTime",
        description = "Login attempt handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Attempt to log into the project registration",
        description = "Attempt to log into the project registration")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = LOGIN_SUCCESSFUL,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT,
                implementation = UserLoginResponse::class))]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = LOGIN_FAILED,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun login(request: UserLoginRequest): Uni<Response> =
        userService.createToken(request.email, request.password)
            .onFailure().recoverWithNull()
            .onItem().transform { pair ->
                if (token != null) {
                    userService.revokeToken(token!!).await().indefinitely()
                }

                val newToken = pair.first
                val user = pair.second

                if (user != null && newToken != null) {
                    log.info("User '${request.email}' logged in")
                    createResponse(UserLoginResponse(newToken, toData(user)),
                        RestStatusInt.OK_200, LOGIN_SUCCESSFUL)
                } else {
                    log.warn("User '${request.email}' failed to login")
                    createResponse(RestStatusInt.UNAUTHORIZED_401, LOGIN_FAILED)
                }
            }

    @POST
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/logout")
    @Counted(name = "logoutCount", description = "Logout count")
    @Timed(name = "logoutTime", description = "Logout handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "Log out of the project registration",
        description = "Log out of the project registration")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = LOGOUT_SUCCESSFUL,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Logging out requires to be logged in",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun logout(): Uni<Response> =
        userService.revokeToken(token!!).onItem().transform { _ ->
            createResponse(RestStatusInt.OK_200, LOGOUT_SUCCESSFUL)
        }

    @GET
    @ReactiveTransactional
    @RolesAllowed(
        UserGroup.STUDENT,
        UserGroup.LECTURER,
        UserGroup.FACULTY_BOARD_OF_EXAMINERS,
        UserGroup.FACULTY_SECRETARIAT_STAFF,
        UserGroup.EXAMINATION_AUTHORITY_STAFF,
        UserGroup.ADMINISTRATOR)
    @Path("/lecturer/paged/{pageIndex}/{pageSize}")
    @Counted(name = "userListLecturerCount",
        description = "User list lecturer count")
    @Timed(name = "userListLecturerTime",
        description = "User list lecturer handling time",
        unit = MetricUnits.MILLISECONDS)
    @Operation(summary = "List lecturers",
        description = "List lecturers")
    @APIResponses(value = [
        APIResponse(responseCode = RestStatusString.OK_200,
            description = USER_FOUND,
            content = [Content(mediaType = MediaType.APPLICATION_JSON,
                schema = Schema(type = SchemaType.OBJECT,
                implementation = UserListResponse::class))]),
        APIResponse(responseCode = RestStatusString.BAD_REQUEST_400,
            description = USER_NOT_EXISTS,
            content = [Content(mediaType = MediaType.APPLICATION_JSON)]),
        APIResponse(responseCode = RestStatusString.UNAUTHORIZED_401,
            description = "Only a student or lecturer may list related projects",
            content = [Content(mediaType = MediaType.APPLICATION_JSON)])])
    fun listLecturers(
        @RestPath pageIndex: Int,
        @RestPath pageSize: Int,
    ): Multi<UserViewResponse> =
        userService.viewLecturers(pageIndex, pageSize)
            .onItem().transform<UserViewResponse> { item -> toData(item) }

    @GET
    @PermitAll
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    @Counted(name = "helloCount",
        description = "How many requests on the Hello resource have been performed.")
    @Timed(name = "helloTime",
        description = "A measure of how long it takes to handle the Hello request.",
        unit = MetricUnits.MILLISECONDS)
    // fun hello() = "Hello project registration!"
    fun hello(): String {
        log.info("hello")
        return "Hello project registration!"
    }

    @GET
    @RolesAllowed(UserGroup.STUDENT)
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    @Counted(name = "testCount", description = "How many requests on the Test resource have been performed.")
    @Timed(name = "testTime", description = "A measure of how long it takes to handle the Test request.", unit = MetricUnits.MILLISECONDS)
    fun test(): String {
        log.debug("test")
        return "Test project registration!"
    }

    companion object {

        private fun toData(user: User) = UserViewResponse(
            userID = user.userID!!,
            email = user.email,
            userType = user.userType.code,
            admin = user.admin,
            facultyBoardOfExaminers = user.facultyBoardOfExaminers,
            facultyAcronyms = user.faculties.map { it.acronym!! },
            departmentAcronyms = user.departments.map { it.acronym!! },
        )

        private const val USER_NOT_EXISTS = "A user with the given id does not exist"
        private const val USER_NOT_FOUND = "No user found"
        private const val USER_FOUND = "User found"
        private const val USER_CREATED = "User created"
        private const val USER_ALREADY_EXISTS = "A user with the given email already exists"
        private const val LOGIN_SUCCESSFUL = "Login successful"
        private const val LOGIN_FAILED = "Email or password invalid"
        private const val LOGOUT_SUCCESSFUL = "Logout successful"
    }
}
