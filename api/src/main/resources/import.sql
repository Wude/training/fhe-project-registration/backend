--------------------------------------------------------------------------------
-- Faculties and Departments
--------------------------------------------------------------------------------

insert into faculties (faculty_acronym, creation_instant, modification_instant, name)
values ('GET', now(), now(), 'Gebäudetechnik und Informatik');

insert into departments (department_acronym,
    creation_instant, modification_instant, name, faculty_acronym)
values ('AI', now(), now(), 'Angewandte Informatik', 'GET');

--------------------------------------------------------------------------------
-- Users and their faculty/department memberships
--------------------------------------------------------------------------------

-- steffen.avemarg@fh-erfurt.de
insert into users (user_id, creation_instant, modification_instant, email, password,
    user_type, admin, faculty_board_of_examiners)
values ('612d3ea1-a0f0-f0a7-a7fc-775746569316', now(), now(),
    'steffen.avemarg@fh-erfurt.de',
    '2a$10$loNsdcdGqC7zkYg0bW7P6.zKpbui4RqyaLYHIMnPWAwZJQ.pbSmFW',
    'LEC', true, true);

insert into user_faculties (user_id, faculty_acronym)
values ('612d3ea1-a0f0-f0a7-a7fc-775746569316', 'GET');

insert into user_departments (user_id, department_acronym)
values ('612d3ea1-a0f0-f0a7-a7fc-775746569316', 'AI');

-- kay.guertzig@fh-erfurt.de
insert into users (user_id, creation_instant, modification_instant, email, password,
    user_type, admin, faculty_board_of_examiners)
values ('6510b80d-5c6a-6a0c-0cce-775746567316', now(), now(),
    'kay.guertzig@fh-erfurt.de',
    '2a$10$loNsdcdGqC7zkYg0bW7P6.zKpbui4RqyaLYHIMnPWAwZJQ.pbSmFW',
    'LEC', false, true);

insert into user_faculties (user_id, faculty_acronym)
values ('6510b80d-5c6a-6a0c-0cce-775746567316', 'GET');

insert into user_departments (user_id, department_acronym)
values ('6510b80d-5c6a-6a0c-0cce-775746567316', 'AI');

-- stefan.woyde@fh-erfurt.de
insert into users (user_id, creation_instant, modification_instant, email, password,
    user_type, admin, faculty_board_of_examiners)
values ('612bc797-83ef-efa7-a782-775746567316', now(), now(),
    'stefan.woyde@fh-erfurt.de',
    '2a$10$loNsdcdGqC7zkYg0bW7P6.zKpbui4RqyaLYHIMnPWAwZJQ.pbSmFW',
    'STU', true, false);

insert into user_faculties (user_id, faculty_acronym)
values ('612bc797-83ef-efa7-a782-775746567316', 'GET');

insert into user_departments (user_id, department_acronym)
values ('612bc797-83ef-efa7-a782-775746567316', 'AI');

--------------------------------------------------------------------------------
-- Projects
--------------------------------------------------------------------------------

-- create table projects (deadline date not null, grade numeric(2,1), supervising_tutor_1_accepted boolean not null, supervising_tutor_2_accepted boolean not null, creation_instant timestamp(6) with time zone not null, modification_instant timestamp(6) with time zone not null, project_id uuid not null, student_id uuid not null, supervising_tutor_1_id uuid not null, supervising_tutor_2_id uuid, department_acronym char(3) not null, faculty_acronym char(3) not null, project_type char(3) not null, status char(3) not null, title varchar(255) not null, student_signature bytea not null, supervising_tutor_1_signature bytea, supervising_tutor_2_signature bytea, primary key (project_id));

insert into projects (creation_instant, modification_instant,
    title, project_type, status, project_id, faculty_acronym, department_acronym,
    student_id, student_signature,
    supervising_tutor_1_id, supervising_tutor_1_accepted, supervising_tutor_2_accepted)
values (now(), now(),
    'Test-BA-Projekt', 'BAP', 'SUB', '6510aab9-9108-08db-db56-775746567316', 'GET', 'AI',
    '612bc797-83ef-efa7-a782-775746567316', ''::bytea,
    '612d3ea1-a0f0-f0a7-a7fc-775746569316', false, false);

insert into projects (creation_instant, modification_instant,
    title, project_type, status, project_id, faculty_acronym, department_acronym,
    student_id, student_signature,
    supervising_tutor_1_id, supervising_tutor_1_accepted,
    supervising_tutor_2_id, supervising_tutor_2_accepted)
values (now(), now(),
    'Test-BA-Abschlussarbeit', 'BAT', 'SUB', '6510aaf1-9462-62a7-a770-775746567316', 'GET', 'AI',
    '612bc797-83ef-efa7-a782-775746567316', ''::bytea,
    '612d3ea1-a0f0-f0a7-a7fc-775746569316', false,
    '6510b80d-5c6a-6a0c-0cce-775746567316', false);
