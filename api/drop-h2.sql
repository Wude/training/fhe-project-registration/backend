drop table if exists departments cascade ;
drop table if exists faculties cascade ;
drop table if exists project_documents cascade ;
drop table if exists projects cascade ;
drop table if exists user_departments cascade ;
drop table if exists user_faculties cascade ;
drop table if exists users cascade ;
